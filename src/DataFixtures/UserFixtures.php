<?php

namespace App\DataFixtures;


use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {
            $user = $this->userHandler->createNewUser([
                'email' => '123@123.ru',
                'IDCard' => 'qwert & qwert',
                'password' => '123321'
            ]);


        $manager->persist($user);

        $user1 = $this->userHandler->createNewUser([
            'email' => '321@321.ru',
            'IDCard' => 'qwert & qwert',
            'password' => '321123'
        ]);
        $manager->persist($user1);

        $manager->flush();


    }
}