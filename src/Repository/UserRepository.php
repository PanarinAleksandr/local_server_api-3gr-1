<?php

namespace App\Repository;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(RegistryInterface $registry, UserHandler $userHandler)
    {
        parent::__construct($registry, User::class);
        $this->userHandler = $userHandler;
    }

    /**
     * @param string $email
     * @param string $plainPassword
     * @return mixed|null
     * @throws \App\Model\Api\ApiException
     */
    public function getBuCredentials( string $email, string $plainPassword)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->andwhere('a.password = :password')
                ->setParameter('email', $email)
                ->setParameter('password', $this->userHandler->encodePlainPassword($plainPassword))
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function getBuCredentialsOnUid(string $uid)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.faceBookId = :faceBookId' )
                ->orWhere('a.googleId = :googleId')
                ->orWhere('a.vkId = :vkId')
                ->setParameter('vkId',$uid)
                ->setParameter('faceBookId',$uid)
                ->setParameter('googleId',$uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $email
     * @param string $IDCard
     * @return mixed|null
     */
    public function getBuCredentialsOnEmailOrIDCard(string $email, string $IDCard)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->orWhere('a.IDCard = :IDCard')
                ->setParameter('email', $email)
                ->setParameter('IDCard', $IDCard)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }


}
