<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homePage")
     */
    public function indexAction(){

        return $this->render('index.html.twig',
            [
                'user' => $this->getUser()
            ]);
    }
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function signUpAction(ApiContext $apiContext, Request $request, ObjectManager $manager, UserHandler $userHandler)
    {
        $user = new User();
        $form = $this->createForm('App\Form\RegisterType', $user);

        $form->handleRequest($request);


        $error = null;

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getIDCard(), $user->getEmail())) {
                    $error = "Вы уже зарегестрированы";
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'IDCard' => $user->getIDCard(),
                        'password' => $user->getPassword(),
                    ];
                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();
                    return $this->redirectToRoute('homePage');
                }
            } catch (ApiException $e) {
                $error = "Error" . $e->getMessage();
            }

        }
        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/auth" , name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function authAction(UserRepository $userRepository,
                               Request $request,
                               UserHandler $userHandler, ApiContext $apiContext,
                               ObjectManager $manager)
    {

        $error = null;
        $form = $this->createForm('App\Form\AuthType');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $user = $userRepository->getBuCredentials($data['email'], $data['password']);


            if ($user) {
                $userHandler->makeUserSession($user);

                return $this->redirectToRoute('homePage');
            }
            try {
                if ($apiContext->checkClientCredentials($data['password'], $data['email'])) {

                    $centralData = $apiContext->getClientByEmail($data['email']);

                    $user = $userHandler->createNewUser($centralData, false);
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('homePage');

                }else{
                    $error = 'Такого пользователя нет в базе данных, зарегестрируйтесь';

                }
            } catch (ApiException $e) {

                $error = 'Не работает';
            }

        }
        return $this->render('auth.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/qwe", name="qwe")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qwe(ApiContext $apiContext)
    {

        $result = $apiContext->clientExists('qwert & qwert', '123@123.ru');


        return new Response(var_export($result, true));
    }
}
