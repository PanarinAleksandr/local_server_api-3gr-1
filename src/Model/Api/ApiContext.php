<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{IDCard}/{email}';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CONCRETE_CLIENT_BY_UID = '/check_client/uid/{uid}';
    const ENDPOINT_CONCRETE_DATA_CLIENT_BY_UID = '/check_client/uid/{uid}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password/encode';

    const ENDPOINT_CLIENT_BIND_SOCIAL_NETWORK = '/bind_social/email/{email}/IDCard/{IDCard}/uid/{uid}/network/{network}';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param null|string $IDCard
     * @param null|string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(?string $IDCard, ?string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'IDCard' => $IDCard,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $email
     * @return mixed
     * @throws ApiException
     */
    public function getClientByEmail(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $plainPassword, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'encodedPassword' => $this->encodePassword($plainPassword) , /*userHandler->encodePlainPassword($plainPassword),*/
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }

    /**
     * @param string $uid
     * @return mixed
     * @throws ApiException
     */
    public function checkClientByUid(string $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_UID, [
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $uid
     * @return mixed
     * @throws ApiException
     */
    public function getClientByUid($uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_DATA_CLIENT_BY_UID, [
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }


    /**
     * @param string $email
     * @param string $IDCard
     * @param string $uid
     * @param string $network
     * @return mixed
     * @throws ApiException
     */
    public function  bindSocialNetworkAction(string $email , string $IDCard ,string $uid , string $network)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CLIENT_BIND_SOCIAL_NETWORK,[
                'email' => $email,
                'uid' => $uid,
                'network' => $network,
                'IDCard' =>$IDCard
            ]
        );
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }
}
