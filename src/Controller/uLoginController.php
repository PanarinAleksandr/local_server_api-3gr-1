<?php


namespace App\Controller;




use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';


    /**
     * @Route("/register-case" , name="app_register_case")
     */
    public function registerCaseAction()
    {
        $u = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($u,true);

//        //$user['network'] - соц. сеть, через которую авторизовался пользователь
//        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
//        //$user['first_name'] - имя пользователя
//        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA ,  $u);

        $user = new User();
        $user->setEmail($userData['email']);
        $form = $this->createForm('App\Form\ULoginRegisterType',$user, [
            'action' => $this
                ->get('router')
                ->generate('app-register-case2')
        ]);




        return $this->render('registration/ul_2_state.html.twig',
            [
                'form'=> $form->createView(),
                'error' => null
            ]
        );

    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {

            try {
                if ($apiContext->clientExists($user->getIDCard(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social----".time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute('homePage');
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("auth_case" , name="app-auth-case")
     * @param UserHandler $userHandler
     * @param UserRepository $userRepository
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function authCaseAction(UserHandler $userHandler, UserRepository $userRepository, ApiContext $apiContext, ObjectManager $manager)
    {
        $u = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($u,true);

        $user = $userRepository->getBuCredentialsOnUid($userData['uid']);

        if ($user){
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('homePage');
        }

        try {

            $checkUserInCentral = $apiContext->checkClientByUid($userData['uid']);

            if($checkUserInCentral){
                $centralUserData =  $apiContext->getClientByUid($userData['uid']);

               $localUser = $userRepository->getBuCredentialsOnEmailOrIDCard($centralUserData['email'],$centralUserData['IDCard']);
                if($localUser){
               switch ($userData['network']){
                   case UserHandler::SOC_NETWORK_VKONTAKTE:
                       $localUser->setVkId($userData['uid']);
                       $manager->persist($localUser);
                       break;
                   case UserHandler::SOC_NETWORK_GOOGLE:
                       $localUser->setGoogleId($userData['uid']);
                       $manager->persist($localUser);
                       break;
                   case UserHandler::SOC_NETWORK_FACEBOOK:
                       $localUser->setFaceBookId($userData['uid']);
                       $manager->persist($localUser);
                       break;
               }
                     $manager->flush();
                    $userHandler->makeUserSession($localUser);
                    return $this->redirectToRoute('homePage');

                }else{

                  $newUser = $userHandler->createNewUser($centralUserData,false);
                    $manager->persist($newUser);
                    $manager->flush();
                    return $this->redirectToRoute('homePage');
                }

            }else{

                $this->addFlash(
                    'error',
                    'Такого пользователя нет в базе данных, зарегестрируйтесь'
                );
            }
        } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    'Ошибка сервера'
                );
        }
        return  $this->redirectToRoute('auth');
    }

    /**
     * @Route("/bind_social_network", name="bind_social_network")
     * @param ApiContext $apiContext
     * @param UserRepository $repository
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function bindSocialNetworkAction(ApiContext $apiContext, UserRepository $repository, ObjectManager $manager, UserHandler $userHandler)
    {
        $u = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($u,true);

        try {
            $checkUserInCentral = $apiContext->checkClientByUid($userData['uid']);


            if($checkUserInCentral){

                $this->addFlash(
                    'error',
                    'Ваш аккаунт уже привязан'
                );
            }else {
                $user = $this->getUser();
                $socialDate = $apiContext->bindSocialNetworkAction($user->getEmail(), $user->getIDCard(), $userData['uid'], $userData['network']);
                $userLocal = $repository->getBuCredentialsOnEmailOrIDCard($socialDate['email'], $socialDate['IDCard']);

                if ($userLocal) {
                    switch ($userData['network']) {
                        case UserHandler::SOC_NETWORK_VKONTAKTE:
                            $userLocal->setVkId($userData['uid']);
                            $manager->persist($userLocal);
                            break;
                        case UserHandler::SOC_NETWORK_GOOGLE:
                            $userLocal->setGoogleId($userData['uid']);
                            $manager->persist($userLocal);
                            break;
                        case UserHandler::SOC_NETWORK_FACEBOOK:
                            $userLocal->setFaceBookId($userData['uid']);
                            $manager->persist($userLocal);
                            break;
                    }
                    $manager->flush();

                    $this->addFlash(
                        'error',
                        'Вы привязали свой аккаунт'
                    );

                    return $this->redirectToRoute('homePage');

                } else {
                    $newUser = $userHandler->createNewUser($socialDate, false);
                    $manager->persist($newUser);
                    $manager->flush();
                    $this->addFlash(
                        'error',
                        'Вы привязали свой аккаунт'
                    );
                    return $this->redirectToRoute('homePage');
                }

            }
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                'Ошибка сервера'
            );
        }
        return $this->redirectToRoute('homePage');

    }

}