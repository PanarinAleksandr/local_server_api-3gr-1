<?php

namespace App\Model\User;


use App\Entity\User;
use App\Model\Api\ApiContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class UserHandler
{

    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";


    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(ContainerInterface $container, ApiContext $apiContext)
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewUser(array $data, bool $encodePassword = true)
    {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setIDCard($data['IDCard']);
        $user->setVkId($data['vkId']??null);
        $user->setFaceBookId($data['faceBookId']??null);
        $user->setGoogleId($data['googleId']??null);

        if($encodePassword) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }

        $user->setPassword($password);

        return $user;
    }

    /**
     * @param string $password
     * @return string
     * @throws \App\Model\Api\ApiException
     */
    public function encodePlainPassword(string $password): string
    {
        return $this->apiContext->encodePassword($password);
    }

    public  function  makeUserSession(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());

        $this->container
            ->get('security.token_storage')
            ->setToken($token);

        $this->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}