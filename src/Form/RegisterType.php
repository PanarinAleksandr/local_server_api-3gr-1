<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, ['label'=> 'Введиет ваш e-mail'])
            ->add('IDCard', TextType::class, ['label'=> 'Введиет данные паспорта'])
            ->add('password', PasswordType::class, ['label'=> 'Пароль'])
            ->add('save', SubmitType::class)
        ;
    }
}