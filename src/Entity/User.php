<?php

namespace App\Entity;

use App\Model\User\UserHandler;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("IDCard")
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(name="email")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", name="IDCard")
     */
    private $IDCard;

    /**
     * @var string
     * @ORM\Column(type="string", name="role")
     */
    private $roles;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="date" )
     */
    private $date;

    /**
     * @ORM\Column(type="string", name="password")
     */
    private $password;
    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $vkId;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $faceBookId;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $googleId;

    public function __construct()
    {
        $this->date = new \DateTime("now");
        $this->roles = json_encode(['ROLE_USER']);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     */
    public function getRoles()
    {
        return json_decode($this->roles, true);
    }

    /**
     * @param $role
     * @return string
     */
    public function addRoles($role)
    {
        $roles = json_decode($this->roles, true);
        $roles[] = $role;
        $this->roles = json_encode($roles);
        return $this->roles;

    }


    public function setRoles(array $roles)
    {
        return json_encode($roles);

    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $IDCard
     * @return User
     */
    public function setIDCard(string $IDCard): User
    {
        $this->IDCard = $IDCard;
        return $this;
    }

    /**
     */
    public function getIDCard()
    {
        return $this->IDCard;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return  $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    public function getSocialId($network) {
        switch($network) {
            case UserHandler::SOC_NETWORK_VKONTAKTE:
                return $this->vkId;
                break;
            case UserHandler::SOC_NETWORK_FACEBOOK:
                return $this->faceBookId;
                break;
            case UserHandler::SOC_NETWORK_GOOGLE:
                return $this->googleId;
                break;
            default:
                return null;
        }
    }

    /**
     * @param mixed $vkId
     * @return User
     */
    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkId()
    {
        return $this->vkId;
    }

    /**
     * @param mixed $faceBookId
     * @return User
     */
    public function setFaceBookId($faceBookId)
    {
        $this->faceBookId = $faceBookId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFaceBookId()
    {
        return $this->faceBookId;
    }

    /**
     * @param mixed $googleId
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }
    
    public function __toArray() {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'IDCard' => $this->IDCard,
            'vkId' => $this->vkId,
            'faceBookId' => $this->faceBookId,
            'googleId' => $this->googleId,
        ];
    }

    public function setSocialId($network, $id) {
        switch($network) {
            case UserHandler::SOC_NETWORK_VKONTAKTE:
                $this->vkId = $id;
                break;
            case UserHandler::SOC_NETWORK_FACEBOOK:
                $this->faceBookId = $id;
                break;
            case UserHandler::SOC_NETWORK_GOOGLE:
                $this->googleId = $id;
                break;
        }
        return $this;
    }
}
